from sympy import *
import sys

def pretty_gs(gs):
    return " \\\\\n".join([pretty_gs_aux(i+1,x) for (i,x) in enumerate(gs)])

def pretty_gs_aux(i,m):
    if i>1:
        return "u_{0} &= v_{0} - \sum_{{i=1}}^{{{1}}} \\frac{{v_{0}u_i}}{{u_iu_i}}u_i = ".format(i,i-1) + latex(m)
    else:
        return "u_1 &= v_1 =" + latex(m)

def pretty_normalize(vectors):
    return " \\\\\n".join(
        ["\\frac{{u_{0}}}{{\sqrt{{u_{0}u_{0}}}}} &= ".format(i+1) + latex(v)
        for (i,v) in enumerate(vectors)]
    )
    

if __name__ == "__main__":
    vs = [[int(e) for e in line.split()] for line in sys.stdin]
    as_mat = [Matrix(v) for v in vs]
    gs = GramSchmidt(as_mat)
    ns = [v.normalized() for v in gs]
    print("\\begin{align*}")
    print(pretty_gs(gs))
    print("\\end{align*}")
    print("\\begin{align*}")
    print(pretty_normalize(ns))
    print("\\end{align*}")
