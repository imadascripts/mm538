MM538
=====

A repo of helpful material for the written MM538 exam

gramschmidt.py
--------------

Requires `sympy` to run. Output latex code which requires the `amsmath`
package.

Reads `n` space separated lists from `stdin`. One per line. Use `Ctrl+D`
to end input (you need to press enter after last line too).

Outputs Gram-Schmidt orthogonalized vectors as latex (+extra formula
stuff) followed by normalized vectors (+extra formula stuff) to
`stdout`.
